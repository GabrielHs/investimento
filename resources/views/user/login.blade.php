<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login | Investindo</title>

    <link rel="stylesheet" href="{{asset('css/stylesheet.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Fredoka+One" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>

    <div>
        <div class="background"></div>
    </div>
        

    <section id="conteudo-view" class="login">

        <h1>Investindo</h1>
        <h3>O nosso gerenciador de investimento</h3>

        {!! Form::open(['route' => 'users.login', 'method' => 'post']) !!}

        <p>Acesse o sistema</p>

        <label>
            {!! Form::text('username', null,['class' => 'input', 'placeholder' => "Usuario"] )!!}
        </label>

        <label>
            {!! Form::password('password', ['placeholder' => "Senha"]) !!}
        </label>

        {!!Form::submit('Entrar', ['class' => 'btn btn-success']) !!}

        {!! Form::close() !!}

    </section>

</body>
</html>
