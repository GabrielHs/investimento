<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UserSocial extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'social_network', 'social_id', 'social_email','social_avatar','user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public  $timestamps = true;
    protected $table = 'users';

}
