<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['uses' => 'Controller@dashboard']);

/*
 *
 * ROTAS PROJETO WEB
 *===================================
 *
 */

Route::get('/login', ['uses' => 'Controller@autenticao']);
Route::post('/login', ['as' => 'users.login','uses' => 'DashboardController@auth']);