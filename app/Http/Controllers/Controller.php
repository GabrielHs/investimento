<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public Function dashboard()
    {
        $titulo = "Dashboard da página";

        return view("Home.dash", ['title' => $titulo]);
    }


    public Function autenticao()
    {

        return view('user.login');

        //echo 'tela de login';
    }


}
