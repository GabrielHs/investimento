<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\UserRepository;
use App\Validators\UserValidator;
use Illuminate\Support\Facades\Auth;
use DB;
use mysql_xdevapi\Exception;

class DashboardController extends Controller
{

    private $repository;
    private $validator;

    public function __construct(UserRepository $repository, UserValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    public function index()
    {
        return  "Aqui caiu na index";
    }

    //Metodo de autenticação
    public function auth(Request $request)
    {

        try
        {

            $data = [
                'email' => $request->get('username'),
                'password' => $request->get('password')
            ];

            $user = DB::table('users')
                ->where('email', $request->input("username"))
                ->orWhere('password', $request->input("password"))
                ->get();
            //dd($user);


            if($user)
            {
                Auth::attempt($data, false);
                $msg = "Autenticado com sucesso";
                return redirect()->route('user.dashboard', $msg);

            }
            else
            {
                $msg = "Erro ao autenticar";
                return redirect()->route('users.login', $msg);
            }

        }
        catch (Exception $ex)
        {
            return $ex->getMessage();
        }


    }

}
