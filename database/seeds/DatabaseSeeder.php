<?php

use Illuminate\Database\Seeder;
use App\Entities\User;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'cfp' => '11122233345',
            'name' => 'Gabriel',
            'birth' => '1980-10-01',
            'gender' => 'M',
            'email' => 'gabriel@gmail.com',
            'password' => bcrypt('123456'),

        ]);

        // $this->call(UsersTableSeeder::class);
    }
}
